/*
 * Copyright © 2022 Bas Nieuwenhuizen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <iostream>
#include <string>
#include <fstream>
#include <cstdint>
#include <vector>
#include <map>
#include <cstring>

#define SQTT_FILE_MAGIC_NUMBER 0x50303042
#define SQTT_FILE_VERSION_MAJOR 1
#define SQTT_FILE_VERSION_MINOR 4

#define SQTT_GPU_NAME_MAX_SIZE 256
#define SQTT_MAX_NUM_SE 32
#define SQTT_SA_PER_SE 2

enum sqtt_version {
	SQTT_VERSION_NONE = 0x0,
	SQTT_VERSION_1_0  = 0x1,
	SQTT_VERSION_1_1  = 0x2,
	SQTT_VERSION_2_0  = 0x3, /* GFX6 */
	SQTT_VERSION_2_1  = 0x4, /* GFX7 */
	SQTT_VERSION_2_2  = 0x5, /* GFX8 */
	SQTT_VERSION_2_3  = 0x6, /* GFX9 */
	SQTT_VERSION_2_4  = 0x7  /* GFX10 */
};

/**
 * SQTT chunks.
 */
enum sqtt_file_chunk_type {
	SQTT_FILE_CHUNK_TYPE_ASIC_INFO = 0,
	SQTT_FILE_CHUNK_TYPE_SQTT_DESC = 1,
	SQTT_FILE_CHUNK_TYPE_SQTT_DATA = 2,
	SQTT_FILE_CHUNK_TYPE_API_INFO = 3,
	SQTT_FILE_CHUNK_TYPE_ISA_DATABASE = 4,
	SQTT_FILE_CHUNK_TYPE_QUEUE_EVENT_TIMINGS = 5,
	SQTT_FILE_CHUNK_TYPE_CLOCK_CALIBRATION = 6,
	SQTT_FILE_CHUNK_TYPE_CPU_INFO = 7,
	SQTT_FILE_CHUNK_TYPE_SPM_DB = 8,
	SQTT_FILE_CHUNK_TYPE_CODE_OBJECT_DATABASE,
	SQTT_FILE_CHUNK_TYPE_CODE_OBJECT_LOADER_EVENTS,
	SQTT_FILE_CHUNK_TYPE_PSO_CORRELATION,
	SQTT_FILE_CHUNK_TYPE_INSTRUMENTATION_TABLE,
	SQTT_FILE_CHUNK_TYPE_COUNT
};

struct sqtt_file_chunk_id {
	enum sqtt_file_chunk_type type : 8;
	int32_t index : 8;
	int32_t reserved : 16;
};

struct sqtt_file_chunk_header {
	struct sqtt_file_chunk_id chunk_id;
	uint16_t minor_version;
	uint16_t major_version;
	int32_t size_in_bytes;
	int32_t padding;
};

/**
 * SQTT file header.
 */
struct sqtt_file_header_flags {
	union {
		struct {
			int32_t is_semaphore_queue_timing_etw : 1;
			int32_t no_queue_semaphore_timestamps : 1;
			int32_t reserved : 30;
		};

		uint32_t value;
	};
};

struct sqtt_file_header {
	uint32_t magic_number;
	uint32_t version_major;
	uint32_t version_minor;
	struct sqtt_file_header_flags flags;
	int32_t chunk_offset;
	int32_t second;
	int32_t minute;
	int32_t hour;
	int32_t day_in_month;
	int32_t month;
	int32_t year;
	int32_t day_in_week;
	int32_t day_in_year;
	int32_t is_daylight_savings;
};

static_assert(sizeof(struct sqtt_file_header) == 56,
	      "sqtt_file_header doesn't match RGP spec");

struct sqtt_file_chunk_sqtt_data {
	struct sqtt_file_chunk_header header;
	int32_t offset; /* in bytes */
	int32_t size; /* in bytes */
};

struct sqtt_file_chunk_spm_db {
   struct sqtt_file_chunk_header header;
   uint32_t flags;
   uint32_t num_timestamps;
   uint32_t num_spm_counter_info;
   uint32_t sample_interval;
};

struct sqtt_spm_counter_info {
   uint32_t block;
   uint32_t instance;
   uint32_t data_offset; /* offset of counter from the beginning of the chunk */
   uint32_t event_index; /* index of counter within the block */
};


std::vector<std::uint8_t> load_data(const std::string& filename)
{
	std::ifstream fin(filename);
	fin.seekg(0, std::ios::end);
	std::vector<std::uint8_t> data(fin.tellg());
	fin.seekg(0, std::ios::beg);
	fin.read((char*)data.data(), data.size());
	return data;
}

int main(int argc, char *argv[])
{
	auto data = load_data(argv[1]);

	unsigned offset = 56;
	unsigned idx = 0;
	while(offset + sizeof(sqtt_file_chunk_header) <= data.size()) {
		struct sqtt_file_chunk_header header;
		std::memcpy(&header, data.data() + offset, sizeof(header));
		if (header.size_in_bytes == 0) {
			std::cerr << " size in bytes = 0\n";
			return 1;
		}
		if (header.size_in_bytes > data.size() - offset) {
			std::cerr << "overflow " << offset << " " << header.size_in_bytes << " " << data.size() << "\n";
			return 1;
		}


		if (header.chunk_id.type == SQTT_FILE_CHUNK_TYPE_SPM_DB) {
			auto off = offset;
			struct sqtt_file_chunk_spm_db spm_db;
			memcpy(&spm_db, data.data() + offset, sizeof(spm_db));
			off += sizeof(spm_db);
			
			std::vector<std::uint64_t> timestamps(spm_db.num_timestamps);
			memcpy(timestamps.data(), data.data() + off, timestamps.size() * 8);
			off += timestamps.size() * 8;
			
			std::vector<sqtt_spm_counter_info> counters(spm_db.num_spm_counter_info);
			memcpy(counters.data(), data.data() + off, counters.size() * sizeof(sqtt_spm_counter_info));
			off += counters.size() * sizeof(sqtt_spm_counter_info);
			
			std::vector<std::vector<uint16_t>> samples(counters.size());
			
			
			for (unsigned i = 0; i < counters.size(); ++i) {
				samples[i].resize(timestamps.size());
				auto off2 = counters[i].data_offset + sizeof(spm_db) + offset;

				memcpy(samples[i].data(), data.data() + off2, samples[i].size() * sizeof(uint16_t));
			}

			std::cout << "timestamps";
			for (unsigned i = 0; i < counters.size(); ++i) {
				std::cout << ", ";
				std::cout << counters[i].block << "_" << counters[i].instance << "_" << counters[i].event_index;
			}
			std::cout << "\n";
			for (unsigned j = 0; j < timestamps.size(); ++j) {
				std::cout << timestamps[j];
				for (unsigned i = 0; i < counters.size(); ++i)
					std::cout << ", " << samples[i][j];
				std::cout << "\n";
			}
		}
		offset += header.size_in_bytes;
	}
  
}

